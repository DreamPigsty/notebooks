cd /opt
mkdir zookeeper
cd zookeeper
wget https://mirrors.tuna.tsinghua.edu.cn/apache/zookeeper/zookeeper-3.6.1/apache-zookeeper-3.6.1-bin.tar.gz
tar -zxvf apache-zookeeper-3.6.1-bin.tar.gz
mv apache-zookeeper-3.6.1-bin zookeeper-3.6.1
cd zookeeper-3.6.1
mkdir data
mkdir logs
echo "2" > data/myid
